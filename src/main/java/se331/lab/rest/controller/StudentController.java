package se331.lab.rest.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController()

    {
        this.students = new ArrayList<>();
        this.students.add(Student.builder().id(1l)
                .studentId("SE-211")
                .name("Zihao")
                .surname("Yu")
                .image("https://www.google.com/imgres?imgurl=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fimages%2F20180828%2Fa35873d85c374e87a86aa152011e7344.jpeg&imgrefurl=http%3A%2F%2Fwww.sohu.com%2Fa%2F250500433_407433&docid=wZR9etFhETp9tM&tbnid=1YKbL1078BHihM%3A&vet=10ahUKEwiiuNGC4JDlAhXJMo8KHfSTBvsQMwhRKAMwAw..i&w=5663&h=3539&bih=939&biw=1127&q=%E5%BD%AD%E4%BA%8E%E6%99%8F&ved=0ahUKEwiiuNGC4JDlAhXJMo8KHfSTBvsQMwhRKAMwAw&iact=mrc&uact=8")
                .gpa(2.9)
                .penAmount(5)
                .description("handsome!!")
                .build());
    }
    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(students);
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent (@RequestBody Student student) {
        student.setId((long) this.students.size()+1);
        this.students.add(student);
        return ResponseEntity.ok(student);
    }


}

